#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

# Set magic variables for current file & dir
# from http://bash3boilerplate.sh/
readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __root="$(cd "$(dirname "${__dir}")" && pwd)"

# set date
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')

# dependencies
source "${__dir}/logging.sh"
source "${__dir}/validation.sh"

# documentation
logging_usage
validation_usage

# debug mode
[[ "${DEBUG:-}" == 'true' ]] && {
  set -o xtrace
  logging_warning "DEBUG mode activated"
} || {
  logging_info "DEBUG mode not activated, can be done by setting \"DEBUG=true\" when calling the script.\n"
}

info=$(cat << EOF
=========================
date: ${__date}
user: ${USER}
file: ${__file}
process id: $$
arguments count: ${#}
arguments supplied: ${@:-no arguments supplied}
=========================
EOF
)

logging_info "${info}"

