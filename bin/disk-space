#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

if [[ "${DEBUG:-}" == "true" ]]; then
  set -o xtrace
fi;

readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')

# dependencies
source "${__dir}/helpers/logging.sh"

usage () {
  info=$(cat << EOF
  =================================
  Show directories ordered by size

  disk-space [show|debug|usage]

  - show [directory to inspect] [number of result to show]
  - debug [directory to inspect] [number of result to show] (with debug trace)
  - usage

  example
  -------
  disk-space show /Users/* 5

  =================================
EOF
)

  logging_info "${info}"
}

stat () {
  info=$(cat << EOF
  =========================
  date: ${__date}
  user: ${USER:-}
  file: ${__file}
  process id: $$
  arguments count: ${#}
  arguments supplied: ${@:-no arguments supplied}
  =========================
EOF
)

  logging_info "${info}"
}

exec () {
  local src="${1:-$HOME}"
  local top="${2:-10}"

  du -sh "$src" 2>/dev/null | sort -rh | head -n "$top" | cat -n
}

options=${1:-}

case "$options" in
        show)
          exec "${2}" "${3}"
        ;;

        debug)
          stat
          exec "$2" "$3"
        ;;

        usage)
          usage
          ;;
       *)
          usage
          ;;
esac
