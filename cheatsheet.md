# Bash Cheatsheet

useful tips when you want to write bash scripts. 

## `sha-bang`

```shell
#!/bin/sh
#!/bin/bash
#!/usr/bin/env bash # more portable
```

## Options

* **set -** : set options
* **set +** : unset options

```shell
# commonly used (set --help)
set -o errexit
set -o pipefail
set -o nounset
set -o verbose
set -o xtrace
```

## Special Characters

### predefined variables

Can be call inside a script.

* **$1 - $9** - The first 9 arguments to the Bash script. (As mentioned above.)
* **$#** - How many arguments were passed to the Bash script.
* **$@** - All the arguments supplied to the Bash script.
* **$?** - The exit status of the most recently run process.
* **\$\$** - The process ID of the current script.
* **$USER** - The username of the user running the script.
* **$HOSTNAME** - The hostname of the machine the script is running on.
* **$SECONDS** - The number of seconds since the script was started.
* **$RANDOM** - Returns a different random number each time is it referred to.
* **$LINENO** - Returns the current line number in the Bash script.

### null command
`:` is a built-in equivalent to 0 (true)

#### `do nothing on then`
```shell
if condition
then : # Do nothing and branch ahead
else # Or else ...
    take-some-action
fi
```

#### function placeholder
```shell
not_empty () {
    :
}
```

#### emptying a file
```shell
: > data.xxx
```

### `()`
it start a subshell. Variables declare on it are not accessible from the outter script.

```shell
( echo "toto", var1="hello" )
```

## Cookbook

### Variables 

#### `helpers`

```shell
readonly __dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly __file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
readonly __base="$(basename ${__file} .sh)"
readonly __date=$(date '+%Y-%m-%d %H:%M:%S')
```

#### `set variable default value`

```shell
$ my_var="${my_var:-default}"
```

#### `avoid unbound variable`

```shell
$ my_var="{my_var:-}"
```

### Conditional statements

#### `set variable depending on a command result`

```shell
$ helm list postgres &&\
    status=installed ||\
    status=not_installed
```

#### `ternary operator`
```shell
(( result = $nb>10?0:1))
```
